package ru.buzanov.tm.dao;

import ru.buzanov.tm.entity.Task;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TaskManager {
    private List<Task> taskList = new ArrayList<>();

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private ProjectManager projectManager = new ProjectManager();

    private int indexBuf;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy", Locale.ENGLISH);

    public void create() throws IOException {
        System.out.println("[TASK CREATE]");
        Task task = new Task();
        System.out.println("ENTER NAME:");
        String buffer = reader.readLine();

        for (Task task1 : taskList) {
            if (task1.getName().equals(buffer)) {
                System.out.println("Task with this name already exist.");
                return;
            }
        }

        task.setName(buffer);
        System.out.println("ENTER TASK START DATE (DD MM YYYY):");

        try {
            task.setStartDate(dateFormat.parse(reader.readLine()));
            System.out.println("ENTER TASK END DATE (DD MM YYYY):");
            task.setEndDate(dateFormat.parse(reader.readLine()));
            System.out.println("ENTER DESCRIPTION:");
            task.setDescription(reader.readLine());
            taskList.add(task);
            System.out.println("[OK]");
        } catch (ParseException e) {
            System.out.println("Wrong format, try again!");
        }
    }

    public void clear() {
        taskList.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void list() {
        System.out.println("[TASK LIST]");

        for (int i = 0; i < taskList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(taskList.get(i).getName());
        }
    }

    public void rename() throws IOException {
        System.out.println("[RENAME TASK]");
        System.out.println("Choose index number of project to rename");

        for (int i = 0; i < taskList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(taskList.get(i).getName());
        }

        try {
            indexBuf = Integer.parseInt(reader.readLine());
            System.out.println("Write new name");
            taskList.get(indexBuf).setName(reader.readLine());
            System.out.println("[TASK RENAMED]");
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Task with index doesn't exist!");
        }
    }

    public void view() throws IOException {
        System.out.println("[VIEW TASK]");
        System.out.println("Choose index number of task");

        for (int i = 0; i < taskList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(taskList.get(i).getName());
        }

        try {
            indexBuf = Integer.parseInt(reader.readLine());
            System.out.println("TASK NAME:");
            System.out.println(taskList.get(indexBuf).getName());
            System.out.println("TASK START DATE:");
            System.out.println(dateFormat.format(taskList.get(indexBuf).getStartDate()));
            System.out.println("TASK END DATE:");
            System.out.println(dateFormat.format(taskList.get(indexBuf).getEndDate()));
            System.out.println("TASK DESCRIPTION:");
            System.out.println(taskList.get(indexBuf).getDescription());
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Project with index doesn't exist!");
        }
    }

    public void remove() throws IOException {
        System.out.println("[REMOVE TASK]");
        System.out.println("Choose index number of task to remove");

        for (int i = 0; i < taskList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(taskList.get(i).getName());
        }

        try {
            taskList.remove(Integer.parseInt(reader.readLine()));
            System.out.println("[TASK REMOVED]");
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Task with index doesn't exist!");
        }
    }
}