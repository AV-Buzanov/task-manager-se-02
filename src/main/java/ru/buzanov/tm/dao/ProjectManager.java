package ru.buzanov.tm.dao;

import ru.buzanov.tm.entity.Project;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProjectManager {
    private List<Project> projectList = new ArrayList<>();

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private int indexBuf;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy", Locale.ENGLISH);

    public List<Project> getProjectList() {
        return projectList;
    }

    public void create() throws IOException {
        Project project = new Project();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String buffer=reader.readLine();

        for (Project project1 : projectList) {
            if (project1.getName().equals(buffer)){
                System.out.println("Project with this name already exist.");
                return;
            }
        }

        project.setName(buffer);
        System.out.println("ENTER PROJECT START DATE (DD MM YYYY):");

        try {
            project.setStartDate(dateFormat.parse(reader.readLine()));
            System.out.println("ENTER PROJECT END DATE (DD MM YYYY):");
            project.setEndDate(dateFormat.parse(reader.readLine()));
            System.out.println("ENTER DESCRIPTION:");
            project.setDescription(reader.readLine());
            projectList.add(project);
            System.out.println("[OK]");
        } catch (ParseException e) {
            System.out.println("Wrong format, try again!");
        }
    }

    public void clear() {
        projectList.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public void list() {
        System.out.println("[PROJECT LIST]");

        for (int i = 0; i < projectList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectList.get(i).getName());
        }
    }

    public void rename() throws IOException {
        System.out.println("[RENAME PROJECT]");
        System.out.println("Choose index number of project to rename");

        for (int i = 0; i < projectList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectList.get(i).getName());
        }

        try {
            indexBuf = Integer.parseInt(reader.readLine());
            System.out.println("Write new name");
            projectList.get(indexBuf).setName(reader.readLine());
            System.out.println("[PROJECT RENAMED]");
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Project with index doesn't exist!");
        }
    }

    public void remove() throws IOException {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Choose index number of project to remove");

        for (int i = 0; i < projectList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectList.get(i).getName());
        }

        try {
            projectList.remove(Integer.parseInt(reader.readLine()));
            System.out.println("[PROJECT REMOVED]");
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Project with index doesn't exist!");
        }
    }

    public void view() throws IOException {
        System.out.println("[VIEW PROJECT]");
        System.out.println("Choose index number of project");

        for (int i = 0; i < projectList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectList.get(i).getName());
        }

        try {
            indexBuf = Integer.parseInt(reader.readLine());
            System.out.println("PROJECT NAME:");
            System.out.println(projectList.get(indexBuf).getName());
            System.out.println("PROJECT START DATE:");
            System.out.println(dateFormat.format(projectList.get(indexBuf).getStartDate()));
            System.out.println("PROJECT END DATE:");
            System.out.println(dateFormat.format(projectList.get(indexBuf).getEndDate()));
            System.out.println("PROJECT DESCRIPTION:");
            System.out.println(projectList.get(indexBuf).getDescription());
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Project with index doesn't exist!");
        }
    }
}