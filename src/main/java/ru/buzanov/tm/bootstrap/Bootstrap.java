package ru.buzanov.tm.bootstrap;

import ru.buzanov.tm.constant.CommandConst;
import ru.buzanov.tm.dao.ProjectManager;
import ru.buzanov.tm.dao.TaskManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Bootstrap {
    public void start() throws IOException {
        boolean out = true;
        String buffer = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ProjectManager projectManager = new ProjectManager();
        TaskManager taskManager = new TaskManager();

        System.out.println("***WELCOME TO TASK MANAGER***");

        while (out) {
            buffer = reader.readLine();

            switch (buffer) {
                case (CommandConst.CREATE_PROJECT):
                    projectManager.create();
                    break;

                case (CommandConst.CLEAR_PROJECT):
                    projectManager.clear();
                    break;

                case (CommandConst.LIST_PROJECT):
                    projectManager.list();
                    break;

                case (CommandConst.RENAME_PROJECT):
                    projectManager.rename();
                    break;

                case (CommandConst.REMOVE_PROJECT):
                    projectManager.remove();
                    break;

                case (CommandConst.VIEW_PROJECT):
                    projectManager.view();
                    break;

                case (CommandConst.CREATE_TASK):
                    taskManager.create();
                    break;

                case (CommandConst.CLEAR_TASK):
                    taskManager.clear();
                    break;

                case (CommandConst.LIST_TASK):
                    taskManager.list();
                    break;

                case (CommandConst.RENAME_TASK):
                    taskManager.rename();
                    break;

                case (CommandConst.VIEW_TASK):
                    taskManager.view();
                    break;

                case (CommandConst.REMOVE_TASK):
                    taskManager.remove();
                    break;

                case (CommandConst.HELP):
                    helpOut();
                    break;

                case (CommandConst.EXIT):
                    out = false;
                    break;
            }
        }
    }

    private static void helpOut() {
        System.out.println(CommandConst.HELP + ": Show all commands.");
        System.out.println(CommandConst.CLEAR_PROJECT + ": Remove all projects.");
        System.out.println(CommandConst.CREATE_PROJECT + ": Create new project.");
        System.out.println(CommandConst.LIST_PROJECT + ": Show all projects.");
        System.out.println(CommandConst.REMOVE_PROJECT + ": Remove selected project.");
        System.out.println(CommandConst.RENAME_PROJECT + ": Rename project");
        System.out.println(CommandConst.VIEW_PROJECT + ": View project information");
        System.out.println(CommandConst.CLEAR_TASK + ": Remove all tasks.");
        System.out.println(CommandConst.CREATE_TASK + ": Create new task.");
        System.out.println(CommandConst.LIST_TASK + ": Show all tasks.");
        System.out.println(CommandConst.REMOVE_TASK + ": Remove selected tasks.");
        System.out.println(CommandConst.RENAME_TASK + ": Rename task");
        System.out.println(CommandConst.VIEW_TASK + ": View task information");
        System.out.println(CommandConst.EXIT + ": Close program");
    }
}
