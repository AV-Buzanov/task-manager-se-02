package ru.buzanov.tm;

import ru.buzanov.tm.bootstrap.Bootstrap;
import java.io.IOException;

/**
 * Task Manager
 */

public class App {
    public static void main(String[] args) throws IOException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}